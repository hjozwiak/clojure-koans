# Clojure Koans

These are my solutions to the Clojure koans. If you get stuck, please feel free to refer to these solutions as guidance. If you have a better way of solving things, please feel free to send in a pull request.
